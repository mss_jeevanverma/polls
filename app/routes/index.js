// Connect to MongoDB using Mongoose
var mongoose = require('mongoose');
var db;
if (process.env.VCAP_SERVICES) {
   var env = JSON.parse(process.env.VCAP_SERVICES);
   db = mongoose.createConnection(env['mongodb-2.2'][0].credentials.url);
} else {
   db = mongoose.createConnection('localhost', 'pollsapp');
}

// Get Poll schema and model
var PollSchema = require('../models/Poll.js').PollSchema;
var DemoSchema = require('../models/demo.js').DemoSchema;
var Poll = db.model('polls', PollSchema);
var Demo = db.model('demo', DemoSchema);
var ObjectId = require('mongoose').Schema.ObjectId; 
// Main application view
exports.index = function(req, res) {
	res.render('index');
};

// Main application view
exports.cast = function(req, res, polls) {
      console.log(polls);
};

// Main application view
exports.castVote = function(req, res) {
      
      ip = req.body.pollIp;
      Poll.update({_id:ObjectId(req.body.poll_id)}, {"server":"MSS"}, {upsert:false, w: 1}, function(err, result) {
         
         res.json(err);
         
      });
      //poll.update({ _id: req.body.poll_id }, {$set: {totalVotes:10}}, { upsert:true }, function(err) { console.log(err); });
      
};

exports.castVote = function(req, res) {
         // enable cors for the save vote api service
         res.header('Access-Control-Allow-Origin', '*');
         res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
         res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
         var poll = new Poll({ question:req.body.question, server: req.body.server, user: req.body.user, count: req.body.count });
         // intercept OPTIONS method
         if ('OPTIONS' == req.method) {
            res.send(200);
         }
         else {
            
            Poll.findById(req.body.poll_id, function(err, poll){
               
               // update poll if already exists
               if ( poll ) {
                  
                  poll.count = req.body.count;
                  poll.user = req.body.user;
                  poll.server = req.body.server;
                  var choice = poll.choices.id(req.body.choice);
                  if ( choice.votes.length > 0 ) {
                     
                     choice.votes[0].count = req.body.count;
                     
                     
                  } else {
                     
                     choice.votes.push({ ip: req.body.server, count: req.body.count });
                     
                  }
		  
                  
                  poll.save(function (err, doc) {
                     if(err || !doc) {
                        res.json({err:true});
                     } else {
                        res.json({'success':true});
                     }
                  });
               // add new poll if do not exist   
               } else {
                  
                  poll.save(function (err, doc) {
                     if(err || !doc) {
                        res.json({err:true});
                     } else {
                        res.json({'success':true});
                     }
                  });
                  
               }
               
            });
         }
};

// JSON API for list of polls
exports.list = function(req, res) {
         // Query Mongo for polls, just get back the question text
         Poll.find(function(error, polls) {
		res.json(polls);
         });
};

// JSON API for list of polls
exports.voteForm = function(req, res) {
         // enable cors for the save vote api service
         res.header('Access-Control-Allow-Origin', '*');
         res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
         res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
         // Query Mongo for polls, just get back the question text
         Poll.findById(req.params.data, '', { lean: true }, function(err, polls){
            
            if (err || !polls) {
               
               res.json({err:true});
               
            } else {
               
               
               if (polls.type=='radio') {
                  var rad="";
                  for ( var i = 0, ln = polls.choices.length; i < ln; i++ ) {
                     rad +="<div class='radio'><label><input type='radio' name='choice' ng-model='poll.userVote' value='"+polls.choices[i]._id+"'>"+polls.choices[i].text+"</label>"+"</div>";   
                  }
                  options = "<label for='pollTopic'>Choices</label><br />"+rad;
                  
               } else if (polls.type=='dropdown') {
                  var opt="";
                  for ( var i = 0, ln = polls.choices.length; i < ln; i++ ) {
                     
                     opt +='<option name="choice" ng-model="poll.userVote" value="'+polls.choices[i]._id+'">"'+polls.choices[i].text+'"</option>';
                  }
                  options ="<label for='pollTopic'>Choices</label><br /><select class='form-control' name='choice' ng-model='poll.userVote'>'"+opt+"'</select><br />";
                  
               } else {
                  
                  var options = "testing";
               }
               
               
               res.send("<div id='wrapper' style='width:50%; margin:0 auto;'>"+
                        "<div id='preview'></div>"+
                        "<div class='page-header'>"+
                        "<h1>Cast Vote</h1>"+
                        "</div><div ng-hide='poll.userVoted'>"+
                        "<form action='http://mastersoftwaretechnologies.com:9601/cast/vote' role='form' id='voteCast' method='POST'>"+
                        "<div class='form-group'>"+
                        "<label for='pollIp'>Server Key</label>"+
                        "<input type='text' ng-model='poll.ip' class='form-control' name='server' id='pollIp' placeholder='Enter Server Key' value='"+req.params.server+"'>"+
                        "</div>"+
                        "<div class='form-group'>"+
                        "<label for='pollUser'>User Key</label>"+
                        "<input type='text' ng-model='poll.user' class='form-control' name='user' id='pollUser' placeholder='Enter User Key' value='"+req.params.user+"'>"+
                        "</div>"+
                        "<div class='form-group'>"+
                        "<label for='pollTopic'>Topic</label>"+
                        "<input type='text' ng-model='poll.question' class='form-control' name='question' id='pollTopic' placeholder='Enter Poll Topic' value='"+polls.question+"'>"+
                        "</div>"+options+"<div class='form-group'>"+
                        "<label for='pollVote'>Vote Value</label>"+
                        "<input type='text' ng-model='poll.votes' class='form-control' name='count' id='pollVotes' placeholder='Enter vote value' value='"+polls.count+"'>"+
                        "</div>"+
                        "<div class='form-group'>"+
                        "<input type='hidden' ng-model='poll.id' class='form-control' name='poll_id' id='pollVotes' value='"+polls._id+"'>"+
                        "</div>"+
                        "</form></div><button id='ytk-response' class='btn btn-primary'>Submit Vote</button>"+
                        "</div></div>");               
            }
            
         });
};

// JSON API for getting a single poll
exports.poll = function(req, res) {
         res.header('Access-Control-Allow-Origin', '*');
         res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
         res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');   
	 // Poll ID comes in the URL
	 var pollId = req.params.id;
	
	 // Find the poll by its ID, use lean as we won't be changing it
	 Poll.findById(pollId, '', { lean: true }, function(err, poll) {
		  if(poll) {
			var userVoted = false,
					userChoice,
					totalVotes = 0;

			// Loop through poll choices to determine if user has voted
			// on this poll, and if so, what they selected
			for(c in poll.choices) {
				var choice = poll.choices[c]; 

				for(v in choice.votes) {
					var vote = choice.votes[v];
					totalVotes++;

					if(vote.ip === (req.header('x-forwarded-for') || req.ip)) {
						userVoted = true;
						userChoice = { _id: choice._id, text: choice.text };
					}
				}
			}

			// Attach info about user's past voting on this poll
			poll.userVoted = userVoted;
			poll.userChoice = userChoice;

			poll.totalVotes = totalVotes;
		
			res.json(poll);
		  } else {
			res.json({error:true});
		  }
	 });
};

// JSON API for creating a new poll
exports.create = function(req, res) {
         console.log(req.body);
         var reqBody = req.body,
			// Filter out choices with empty text
			choices = reqBody.choices.filter(function(v) { return v.text != ''; }),
			// Build up poll object to save
			pollObj = {question: reqBody.question, choices: choices, server: '', user: '', count: '', type: reqBody.type};
				
         // Create poll model from built up poll object
         var poll = new Poll(pollObj);
	
         // Save poll to DB
         poll.save(function(err, doc) {
		if(err || !doc) {
			console.log(err)
		} else {
			res.json(doc);
		}		
         });
};

exports.vote = function(socket) {
	socket.on('send:vote', function(data) {
		var ip = socket.handshake.headers['x-forwarded-for'] || socket.handshake.address.address;
		
		Poll.findById(data.poll_id, function(err, poll) {
			var choice = poll.choices.id(data.choice);
			choice.votes.push({ ip: ip });
			
			poll.save(function(err, doc) {
				var theDoc = { 
					question: doc.question, _id: doc._id, choices: doc.choices, 
					userVoted: false, totalVotes: 0 
				};

				// Loop through poll choices to determine if user has voted
				// on this poll, and if so, what they selected
				for(var i = 0, ln = doc.choices.length; i < ln; i++) {
					var choice = doc.choices[i]; 

					for(var j = 0, jLn = choice.votes.length; j < jLn; j++) {
						var vote = choice.votes[j];
						theDoc.totalVotes++;
						theDoc.ip = ip;

						if(vote.ip === ip) {
							theDoc.userVoted = true;
							theDoc.userChoice = { _id: choice._id, text: choice.text };
						}
					}
				}
				
				socket.emit('myvote', theDoc);
				socket.broadcast.emit('vote', theDoc);
			});			
		});
	});
};

// Parveen sachdeva
exports.imageupload = function(fs,path,dbPoll) { 
 return function(req, res) {
 	var datetime = new Date().toISOString().replace(/T/, ' ').replace(/\..+/, '').split(" ");
 	var name=req.body.Name;
 	var date=req.body.Date;
 	var categoty=req.body.Category;
	fs.readFile(req.files.fu_File.path, function (err, data) {
	var orgFileName=req.files.fu_File.originalFilename;
	var fileSize=	req.files.fu_File.size;var type=orgFileName.lastIndexOf('.');
	if(type<0){
		type='';
	}
	else{
		type=orgFileName.substr(type);
	}
	if(type!='.jpg' && type!='.gif' && type !='.png' && type !='.jpeg'){
		res.json({'err':'File not in correct format.','status':'0'});
	}
	else{
	if(fileSize>0){
	var sizeKb=fileSize / 1024;
	if(sizeKb<5120){
	if(orgFileName.indexOf(" ") != 0){
	orgFileName=orgFileName.replace(' ','_');
	}
	var newPath = path.resolve('./public/uploads/'+datetime[0]+'_'+orgFileName);
	console.log(newPath);
    fs.writeFile(newPath, data, function (err) {
    	if(!err){
    		var collection=dbPoll.get('UPLOADS');
    	collection.insert({'NAME':name,'DATE':date,'Category':categoty,'IMAGENAME':datetime[0]+'_'+orgFileName});
	res.json({'path':datetime[0]+'_'+orgFileName,'date':datetime[0],'success':'1'});
	    	}
	    	else{
	    		res.json({'err':'Please try again later.','status':'0'});
	    	}
	    });
    }
		else{
		res.json({'err':'File size is more then 5 mb.','status':'0'});
			}
		}
		else{
			res.json({'err':'No file found.','status':'0'});
			}
		}
	  });

	};
};

// Parveen sachdeva 
exports.imagelist = function(dbPoll) {
    return function(req, res) {     
        
        var collection = dbPoll.get('UPLOADS');
        collection.find({$query: {
            
        }, $orderby: { _id : -1 } },function(e,docs){

            res.render('imagelist', {
                "imagelist" : docs
            });
        });    
         /*res.render('imagelist', {
                "imagelist" : "docs"});*/
    };
    
};