var mongoose = require('mongoose');

// Document schema for polls
exports.DemoSchema = new mongoose.Schema({
	
	server: { type: String, required: true },
	user: { type: String, required: true },
	topic: { type: String, required: true },
	vote: String
	
});
